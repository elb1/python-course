---
title: 'Chapter 2: Introduction to Pandas'
description:
  'This chapter will introduce you to the basic concepts of the Pandas library.'
prev: /chapter1
next: /chapter3
type: chapter
id: 2
---

<exercise id="1" title="Creating, reading and writing" type="slides">

<slides source="pd_creating_reading_writing">
</slides>

</exercise>

<exercise id="2" title="Indexing, selecting and assigning" type="slides">

<slides source="pd_indexing_selecting_assigning"></slides>

</exercise>

<exercise id="3" title="Summary functions and maps" type="slides">

<slides source="pd_summary_functions_and_maps"></slides>

</exercise>

<exercise id="4" title="Grouping and sorting" type="slides">

<slides source="pd_grouping_and_sorting"></slides>

</exercise>

<exercise id="5" title="Data types and missing data" type="slides">

<slides source="pd_data_types_and_missing_data"></slides>

</exercise>

<exercise id="6" title="Combining" type="slides">

<slides source="pd_renaming_and_combining"></slides>

</exercise>
