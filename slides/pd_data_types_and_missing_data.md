---
type: slides
---

# Data types and missing data

---

# Data types

```python
df = pd.DataFrame({
    'A': 1.,
    'B': pd.date_range('20190606', periods=4, freq='BM'),  # business month end
    'C': pd.Series(1, index=list(range(4)), dtype='float32'),
    'D': np.array([3] * 4, dtype='int32'),
    'F': 'foo'})
df
```


```out
     A          B    C  D    F
0  1.0 2019-06-28  1.0  3  foo
1  1.0 2019-07-31  1.0  3  foo
2  1.0 2019-08-30  1.0  3  foo
3  1.0 2019-09-30  1.0  3  foo
```
```python
df.dtypes
```


```out
A           float64
B    datetime64[ns]
C           float32
D             int32
F            object
dtype: object
```

---

# Missing data

```python
df = pd.DataFrame({'A': [1, None, 2], 'B': [3, None, None]})
df
```

```out
     A    B
0  1.0  3.0
1  NaN  NaN
2  2.0  NaN
```

## fillna

```python
df.fillna(0)
```

```out
     A    B
0  1.0  3.0
1  0.0  0.0
2  2.0  0.0
```

---


# backward and forward fill

```python
df.bfill()
```

```out
     A    B
0  1.0  3.0
1  2.0  NaN
2  2.0  NaN
```


```python
df.ffill()
```

```out
     A    B
0  1.0  3.0
1  1.0  3.0
2  2.0  3.0
```


---

# <a href="https://www.kaggle.com/kernels/fork/598826" target="_blank">Now go solve these exercises</a>
