---
type: slides
---

# Grouping and sorting

---

# Grouping and aggregations

By “group by” we are referring to a process involving one or more of the following steps:

* **Splitting** the data into groups based on some criteria
* **Applying** a function to each group independently
* **Combining** the results into a data structure

```python
df = pd.DataFrame({
    'currency': ['DKK', 'NOK', 'DKK', 'DKK', 'NOK', 'SEK', 'SEK'],
    'product': ['shoes', 'pants', 'shoes', 'shirts', 'shirts', 'hat', 'hat'],
    'price': [295, 895, 995, 150, 700, 195, 30],
    'discount': [15, 0, 95, 25, 75, 45, 95]
})
df
```

```out
  currency product  price  discount
0      DKK   shoes    295        15
1      NOK   pants    895         0
2      DKK   shoes    995        95
3      DKK  shirts    150        25
4      NOK  shirts    700        75
5      SEK     hat    195        45
6      SEK     hat     30        95
```

---


# Group by

Grouping and then applying the `sum()` function to the resulting groups.


```python
df.groupby('product').sum()
```

```out
         price  discount
product                 
hat        225       140
pants      895         0
shirts     850       100
shoes     1290       110
```

Grouping by multiple columns forms a hierarchical index, and again we can apply the sum function.


```python
df.groupby(['currency', 'product']).sum()
```

```out
                  price  discount
currency product                 
DKK      shirts     150        25
         shoes     1290       110
NOK      pants      895         0
         shirts     700        75
SEK      hat        225       140
```

# Group by

Use `.agg` to apply different group functions


```python
df.groupby('product').agg({
    'price': 'sum',
    'discount': 'max',
    'currency': lambda group: any(v == 'DKK' for v in group)
}).rename(columns={'currency': 'in_dkk', 'price': 'sum_price', 'discount': 'max_discount'})
```

```out
         sum_price  max_discount  in_dkk
product                                 
hat            225            95   False
pants          895             0   False
shirts         850            75    True
shoes         1290            95    True
```

---

# Reshaping

The `stack()` method “compresses” a level in the DataFrame’s columns.

```python
df.set_index(['currency', 'product'], inplace=True)
df
```

```out
                  price  discount
currency product                 
DKK      shoes      295        15
NOK      pants      895         0
DKK      shoes      995        95
         shirts     150        25
NOK      shirts     700        75
SEK      hat        195        45
         hat         30        95
```

```python
df.stack()
```

```out
currency  product          
DKK       shoes    price       295
                   discount     15
NOK       pants    price       895
                   discount      0
DKK       shoes    price       995
                   discount     95
          shirts   price       150
                   discount     25
NOK       shirts   price       700
                   discount     75
SEK       hat      price       195
                   discount     45
                   price        30
                   discount     95
```

Notes:

The inverse operation of `stack()` is `unstack()`, which by default unstacks the *last level*. Note it requires the 
index values to be unique within each level.

---

# Sorting

```python
df.reset_index().sort_values(['currency', 'price'], ascending=[True, False])
```


```out
  currency product  price  discount
2      DKK   shoes    995        95
0      DKK   shoes    295        15
3      DKK  shirts    150        25
1      NOK   pants    895         0
4      NOK  shirts    700        75
5      SEK     hat    195        45
6      SEK     hat     30        95
```

---

# <a href="https://www.kaggle.com/kernels/fork/598715" target="_blank">Now go solve these exercises</a>
