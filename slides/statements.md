---
type: slides
---

# Statements


---

# Control Flow

In the programs we have seen till now, there has always been a series of statements faithfully executed by Python in exact top-down order. What if you wanted to change the flow of how it works? For example, you want the program to take some decisions and do different things depending on different situations, such as printing 'Good Morning' or 'Good Evening' depending on the time of the day?

As you might have guessed, this is achieved using control flow statements. There are three control flow statements in Python - `if`, `for` and `while`.


---

# The `if` statement

The `if` statement is used to check a condition: if the condition is true, we run a block of statements (called the if-block), else we process another block of statements (called the else-block). The else clause is optional.

```python
number = 23
if 0 < number < 30:
    print('The number must be positive and less than 30.')
elif 30 <= number:
    print('It must be a big number')
else:
    print('I suppose the number is negative')
print('Done')
```

```out
The number must be positive and less than 30.
Done
```


---

# The `for` loop

The `for` loop is a looping statement which iterates over a sequence of objects i.e. go through each item in a sequence. A for statement can have an optional else clause.

```python
for i in range(5):
    print(i)
else:
    print('The for loop is over')
```

```out
0
1
2
3
4
The for loop is over
```

Notes:

Note that the `else` part is optional. When included, it is always executed once after the for loop is over unless a `break` statement is encountered.

---

# The `while` statement

The `while` statement allows you to repeatedly execute a block of statements as long as a condition is true. A while statement can have an optional else clause.

```python
number = 2
running = True
guess = 0
while running:
    if guess == number:
        print('Congratulations, you guessed it.')
        running = False
    elif guess < number:
        print('No, it is a little higher than {}.'.format(guess))
    guess += 1
else:
    print('The while loop is over.')
print('Done')
```

```out
No, it is a little higher than 0.
No, it is a little higher than 1.
Congratulations, you guessed it.
The while loop is over.
Done
```


---

# The `break` statement

The `break` statement is used to break out of a loop statement i.e. stop the execution of a looping statement, even if the loop condition has not become False or the sequence of items has not been completely iterated over.


```python
number = 2
running = True
guess = 0
while True:
    if guess == number:
        print('Congratulations, you guessed it.')
        break
    elif guess < number:
        print('No, it is a little higher than {}.'.format(guess))
    guess += 1
else:
    print('The while loop is over.')
print('Done')
```

```out
No, it is a little higher than 0.
No, it is a little higher than 1.
Congratulations, you guessed it.
Done
```


Notes:

An important note is that if you break out of a for or while loop, any corresponding loop else block is not executed.


---

# The `continue` statement

The `continue` statement is used to tell Python to skip the rest of the statements in the current loop block and to continue to the next iteration of the loop.

```python
number = 2
running = True
guess = -5
while True:
    if guess < 0:
        guess += 2
        continue
        
    if guess == number:
        print('Congratulations, you guessed it.')
        break
    elif guess < number:
        print('No, it is a little higher than {}.'.format(guess))
    guess += 1
print('Done')
```

```out
No, it is a little higher than 1.
Congratulations, you guessed it.
Done
```



---

# <a href="https://www.kaggle.com/kernels/fork/1275165" target="_blank">Lets do some exercises</a>
